const getElement = (values) => {
  const roll = [];
  let suma = 0;
  for(let i=0; i<values.length; i++){
    suma += values[i][1];
    for(let j=0; j<values[i][1]*100; j++){
      roll.push(values[i][0]);
    }
  }
  if(suma!==100&&suma<99.98){
    console.log(suma)
    throw new Error('100% NOT FOUND. Check probability...');
  }
  const rng = Math.floor(Math.random()*10000)
  //console.log('Got RNG', rng/100);
  return roll[rng];
}

const createElement = (parentFolder, RNG, zIndex) => {
  document.getElementById("marker").insertAdjacentHTML("afterend", `<img src="img/${parentFolder}/Ilustración_sin_título-${RNG}.png" class="layer" style="z-index: ${zIndex};">`);
  console.log('Added element', parentFolder, RNG);
  return 'done';
}