const manual = () => {
  addListeners();
  createImg();
}

const createImg = () => {

  // Funcion para obtener valores especificos
  const getSpecial = (param, value, name) => {
    if(param === value){
      alert(`Encontrado ${name} de valor ${value}!`);
    }else{
      location.reload();
    }
  }

  // Dependencias
  let rngSombreros = getElement(rarezaSombreros);
  let rngAccesoriosOjos = getElement(rarezaAccesoriosOjos);
  let rngPelo = getElement(rarezaPelo);
  let rngOjos = getElement(rarezaOjos);
  let rngFondo = getElement(rarezaFondos);
  let rngPajaros = getElement(rarezaPajaros);

  if(rngPelo === 1){ // Si es pelado
    let goOn = true;
    while(goOn){ // Solo puede tener bandana o nada
      rngSombreros = getElement(rarezaSombreros);
      if(rngSombreros === 19 || rngSombreros <= 3){
        goOn = false;
      }
    }
  }
  if(rngSombreros <= 3){ // Si tiene Bandana
    while(rngAccesoriosOjos === 9){ // No puede bandana en los ojos
      rngAccesoriosOjos = getElement(rarezaAccesoriosOjos);
    }
  }
  if(rngOjos === 1 && (rngAccesoriosOjos < 4 || rngAccesoriosOjos > 7)){ // Si tiene Ojos especiales
    while(rngAccesoriosOjos < 4 || rngAccesoriosOjos > 7){ // Solo puede transparentes en los ojos
      rngAccesoriosOjos = getElement(rarezaAccesoriosOjos);
      if(rngAccesoriosOjos === 17){
        break;
      }
    }
  }
  if(rngAccesoriosOjos === 9 && rngSombreros <= 3){ // No puede tener 2 bandanas
    while(rngSombreros <= 3){
      rngSombreros = getElement(rarezaSombreros);
    }
  }
  if([9, 10, 15, 16].indexOf(rngAccesoriosOjos) !== -1 && (rngOjos < 1 || rngOjos > 5)){ // Si hay accesorio que tapa
    while(rngOjos < 1 || rngOjos > 5){ // Ojos comunes
      rngOjos = getElement(rarezaOjos);
    }
  }
  if(rngFondo === 3){ // Si hay fondo de espacio
    while(rngPajaros >= 7 && rngPajaros <= 10){ // No puede pajaros volando
      rngPajaros = getElement(rarezaPajaros);
    }
  }

  
  // Creacion de layers
  createElement('Fondo', rngFondo, 0);
  createElement('Piel', getElement(rarezaPiel), 1);
  createElement('Pelo', rngPelo, 5);
  createElement('Ojos', rngOjos, 2);
  createElement('Boca', getElement(rarezaBoca), 2);
  createElement('Barba', getElement(rarezaBarba), 3);
  createElement('Aros', getElement(rarezaAros), 4);
  createElement('Collar', getElement(rarezaCollar), 4);
  createElement('Accesorios ojos', rngAccesoriosOjos, (rngAccesoriosOjos === 10 && (rngSombreros <= 3 || rngSombreros === 7 || rngSombreros === 8)) ? 9 : 7);
  createElement('Pajaros y pelotas', rngPajaros, 10);
  createElement('Ropa', getElement(rarezaRopa), 2);
  createElement('Sombreros', rngSombreros, 8);
  createElement('Textura piel', getElement(rarezaTexturas), 2);
  createElement('Accesorios boca', getElement(rarezaAccesoriosBocas), 9);
  createElement('Nariz', getElement(rarezaNariz), 2);
}

const deleteImg = () => {
  const elements = document.getElementsByClassName('layer');
  while(elements.length > 0){
    elements[0].parentNode.removeChild(elements[0]);
  }
}

const addListeners = () => {
  document.getElementById("refresh").addEventListener("click", function() {
    location.reload();
  });

  document.getElementById("download").addEventListener("click", function() {
    download('nftDownload.png');
  });

  document.getElementById("download5").addEventListener("click", function() {
    downloadMany(5);
  });

  document.getElementById("downloadMany").addEventListener("click", function() {
    downloadMany();
  });
}

const download = fileName => {
  html2canvas(document.querySelector('#capture'), {backgroundColor: null}).then(function(canvas) {
    saveAs(canvas.toDataURL(), fileName);
  });
}

const downloadMany = async(params) => {
  const times = params || prompt('Cantidad a generar:');
  for(let i=0; i<times; i++){
    deleteImg();
    createImg();
    download(`nftDownload-${i}.png`);
    await setTimeout(()=>{
      console.log(`Downloaded nft ${i+1}...`);  
    }, 5000);
  }
}

const saveAs = (uri, filename) => {
  var link = document.createElement('a');
  if (typeof link.download === 'string') {
    link.href = uri;
    link.download = filename;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  } else {
    window.open(uri);
  }
}

// Start script
manual();